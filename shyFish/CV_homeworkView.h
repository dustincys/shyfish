﻿// CV_homeworkView.h : interface of the CCV_homeworkView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CV_HOMEWORKVIEW_H__BA3EFC6F_ED13_4D57_B987_21034DD293D7__INCLUDED_)
#define AFX_CV_HOMEWORKVIEW_H__BA3EFC6F_ED13_4D57_B987_21034DD293D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCV_homeworkView : public CFormView
{
protected: // create from serialization only
	CCV_homeworkView();
	DECLARE_DYNCREATE(CCV_homeworkView)

public:
	//{{AFX_DATA(CCV_homeworkView)
	enum{ IDD = IDD_CV_HOMEWORK_FORM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:
	CCV_homeworkDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCV_homeworkView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	bool m_bisscared;
	int m_i_intimate;
	int m_i_shy_speed;
	int m_i_bkthreshold;
	bool m_bistrace;
	BYTE *m_bkgImageBuffer;
	BYTE *m_pGrayImageBuffer;
	int m_i_orientation;
	void pic_move(CRect &rect);
	int m_i_speed;
	CSliderCtrl m_slider;
	CSliderCtrl m_slider_shy;
	CSliderCtrl m_slider_intimate;
	CSliderCtrl m_slider_bkthreshold;
	CPoint m_cpoint_old_tl_pic;
	CPoint m_cpoint_rightbottom_pic;
	CPoint m_cpoint_lefttop_pic;
	void bitMapPaint();
	BITMAPINFO *m_pBmpInfo;
	char m_chBmpBuf[2048];		//BIMTAPINFO 存储缓冲区，m_pBmpInfo即指向此缓冲区
	HWND m_ghShowWnd;
	virtual ~CCV_homeworkView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCV_homeworkView)
	afx_msg void OnButtonBegin();
	afx_msg LRESULT OnVideoExChange(WPARAM wParam, LPARAM lParam);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnButtonSetbktrace();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CV_homeworkView.cpp
inline CCV_homeworkDoc* CCV_homeworkView::GetDocument()
   { return (CCV_homeworkDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CV_HOMEWORKVIEW_H__BA3EFC6F_ED13_4D57_B987_21034DD293D7__INCLUDED_)
