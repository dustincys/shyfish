﻿// CV_homeworkDoc.cpp : implementation of the CCV_homeworkDoc class
//

#include "stdafx.h"
#include "CV_homework.h"

#include "CV_homeworkDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkDoc

IMPLEMENT_DYNCREATE(CCV_homeworkDoc, CDocument)

BEGIN_MESSAGE_MAP(CCV_homeworkDoc, CDocument)
	//{{AFX_MSG_MAP(CCV_homeworkDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkDoc construction/destruction

CCV_homeworkDoc::CCV_homeworkDoc()
{
	// TODO: add one-time construction code here

}

CCV_homeworkDoc::~CCV_homeworkDoc()
{
}

BOOL CCV_homeworkDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkDoc serialization

void CCV_homeworkDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkDoc diagnostics

#ifdef _DEBUG
void CCV_homeworkDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCV_homeworkDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkDoc commands
