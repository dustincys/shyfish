﻿// CV_homeworkView.cpp : implementation of the CCV_homeworkView class
//

#include "stdafx.h"
#include "CV_homework.h"

#include "CV_homeworkDoc.h"
#include "CV_homeworkView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "vfw.h"
#include "math.h"
#include <iostream.h>
#pragma comment (lib,"vfw32")
/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkView
BYTE *m_pImageBuffer;
int imagewidth = 320, imageheight =240 ,imagesize = 320*240*3;
LRESULT CALLBACK FrameCallbackProc(HWND hWnd, LPVIDEOHDR lpVHdr)
{ 
	 
	if ( m_pImageBuffer!=NULL)
	{ 
		memcpy( m_pImageBuffer , lpVHdr->lpData, imagesize);  
	    AfxGetMainWnd()->SendMessage(WM_VIDEO_EX_CHANGE, 0, 0);
	}
	return 0;
} 
LRESULT CCV_homeworkView::OnVideoExChange(WPARAM wParam, LPARAM lParam)
{ 
	
	if ( m_pImageBuffer!=NULL)
	{
      	CStatic*  pPicture1 =(CStatic*)GetDlgItem(IDC_STATIC_SHOW);
	 	CDC *pDC = pPicture1->GetDC();	
		CRect rect;
		pPicture1->GetWindowRect(&rect);
		ScreenToClient(&rect);
		HDRAWDIB hd;
		hd = DrawDibOpen();
		DrawDibDraw(
			hd, 
			pDC->GetSafeHdc(),
			0, 
			0,
			rect.Width(),
			rect.Height() ,
			&m_pBmpInfo->bmiHeader,
			m_pImageBuffer,//lpVHdr->lpData,
			0, 0,
			m_pBmpInfo->bmiHeader.biWidth, m_pBmpInfo->bmiHeader.biHeight,DDF_SAME_HDC
		);		
		DrawDibClose(hd);		
		if(m_bistrace)
		{
			if(m_pGrayImageBuffer)
				delete[] m_pGrayImageBuffer;
			m_pGrayImageBuffer = new BYTE[imagesize/3];
	        
			for(int  i=0;i<imagesize; i+=3)
			{
			   m_pGrayImageBuffer[i/3] = BYTE(m_pImageBuffer[i]/3.0 +m_pImageBuffer[i+1]/3.0+m_pImageBuffer[i+2]/3.0) ;
			
			}

			//UpdateData();
			for(  i=0;i<imagesize/3 ;i++)
			{
				if(abs(m_pGrayImageBuffer[i] - m_bkgImageBuffer[i])<m_i_bkthreshold)
					m_pGrayImageBuffer[i]  = 255; 
				else
					m_pGrayImageBuffer[i]  = 0; 
			
			} /*二值化*/
		}
		pic_move(rect);
	//
	} 
	return 0;
}

IMPLEMENT_DYNCREATE(CCV_homeworkView, CFormView)

BEGIN_MESSAGE_MAP(CCV_homeworkView, CFormView)
	//{{AFX_MSG_MAP(CCV_homeworkView)
	ON_BN_CLICKED(IDC_BUTTON_BEGIN, OnButtonBegin)
	ON_MESSAGE(WM_VIDEO_EX_CHANGE, OnVideoExChange) 
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_BUTTON_SETBKTRACE, OnButtonSetbktrace)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkView construction/destruction

CCV_homeworkView::CCV_homeworkView()
	: CFormView(CCV_homeworkView::IDD)
{
	//{{AFX_DATA_INIT(CCV_homeworkView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	srand((unsigned)time(NULL));
	m_i_orientation=1;
	m_cpoint_lefttop_pic=0;
	m_cpoint_rightbottom_pic.x=m_cpoint_lefttop_pic.x+20;
	m_cpoint_rightbottom_pic.y=m_cpoint_lefttop_pic.y+20;
	m_cpoint_old_tl_pic=m_cpoint_lefttop_pic;

	m_i_speed=5;
	m_i_shy_speed=80;
	m_i_intimate=100;
	m_i_bkthreshold=60;
	m_bkgImageBuffer=NULL;	
	m_pGrayImageBuffer=NULL;
	m_bistrace=false;
	m_bisscared=false;
}

CCV_homeworkView::~CCV_homeworkView()
{
	if(m_bkgImageBuffer){
		delete[] m_bkgImageBuffer;
	}
	if(m_pGrayImageBuffer){
		delete[] m_pGrayImageBuffer;
	}	
}

void CCV_homeworkView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCV_homeworkView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_SLIDER_SPEED, m_slider);

	DDX_Control(pDX, IDC_SLIDER_SHY, m_slider_shy);

	DDX_Control(pDX, IDC_SLIDER_INTIMATE, m_slider_intimate);
	DDX_Control(pDX, IDC_SLIDER_BKTHRESHOLD, m_slider_bkthreshold);


}

BOOL CCV_homeworkView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CCV_homeworkView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	CWnd* mywnd = GetDlgItem(IDC_STATIC_SHOW);
	CRect rect;
	mywnd->GetWindowRect(&rect);
	rect.SetRect(0,0,rect.Width(),rect.Height());
	m_ghShowWnd=capCreateCaptureWindow( "My Own Capture Window",WS_CHILD|WS_VISIBLE,0, 0, 
		         (rect.right-rect.left), (rect.bottom-rect.top), mywnd->GetSafeHwnd(), 1235);

	m_slider.SetRange(5,100);//设置滑动范围
	m_slider.SetTicFreq(10);//每10个单位画一刻度

	m_slider_shy.SetRange(5,100);//设置滑动范围
	m_slider_shy.SetTicFreq(10);//每10个单位画一刻度
	m_slider_shy.SetPos(80);

	m_slider_intimate.SetRange(1,400);//设置滑动范围
	m_slider_intimate.SetTicFreq(10);//每10个单位画一刻度
	m_slider_intimate.SetPos(100);

	m_slider_bkthreshold.SetRange(10,200);
	m_slider_bkthreshold.SetTicFreq(10);
	m_slider_bkthreshold.SetPos(60);
}

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkView printing

BOOL CCV_homeworkView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCV_homeworkView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCV_homeworkView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CCV_homeworkView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkView diagnostics

#ifdef _DEBUG
void CCV_homeworkView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCV_homeworkView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCV_homeworkDoc* CCV_homeworkView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCV_homeworkDoc)));
	return (CCV_homeworkDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkView message handlers

void CCV_homeworkView::OnButtonBegin() 
{
	// TODO: Add your control notification handler code here
	capDriverConnect (m_ghShowWnd, 0);
	DWORD dwSize;
    dwSize = capGetVideoFormatSize(m_ghShowWnd);
    
	LPBITMAPINFO lpbi;

	lpbi = (LPBITMAPINFO)GlobalAlloc  (GHND, dwSize);
    GlobalLock(lpbi);
    capGetVideoFormat(m_ghShowWnd, lpbi, dwSize); 

    m_pBmpInfo = (BITMAPINFO *)m_chBmpBuf; 
	 
	//	初始化BITMAPINFO 结构，此结构在保存bmp文件、显示采集图像时使用
	m_pBmpInfo->bmiHeader.biSize			= lpbi->bmiHeader.biSize;

	//	图像宽度，一般为输出窗口宽度
	m_pBmpInfo->bmiHeader.biWidth			=  lpbi->bmiHeader.biWidth;		
	 
	m_pBmpInfo->bmiHeader.biHeight			=  lpbi->bmiHeader.biHeight;		
	m_pBmpInfo->bmiHeader.biBitCount		= lpbi->bmiHeader.biBitCount ;
 
	m_pBmpInfo->bmiHeader.biPlanes			=  lpbi->bmiHeader.biPlanes;	
	m_pBmpInfo->bmiHeader.biCompression		=  lpbi->bmiHeader.biCompression;
	m_pBmpInfo->bmiHeader.biSizeImage		= lpbi->bmiHeader.biSizeImage;
	m_pBmpInfo->bmiHeader.biXPelsPerMeter	= lpbi->bmiHeader.biXPelsPerMeter;
	m_pBmpInfo->bmiHeader.biYPelsPerMeter	= lpbi->bmiHeader.biYPelsPerMeter;
	m_pBmpInfo->bmiHeader.biClrUsed			= lpbi->bmiHeader.biClrUsed;
	m_pBmpInfo->bmiHeader.biClrImportant	= lpbi->bmiHeader.biClrImportant; 
    

    GlobalUnlock(lpbi);

	GlobalFree(lpbi);

    imagewidth = m_pBmpInfo->bmiHeader.biWidth;
	imageheight = m_pBmpInfo->bmiHeader.biHeight;
	imagesize  = m_pBmpInfo->bmiHeader.biSizeImage;
	/*char str[20];
	sprintf(str, "%d", imagewidth);
	MessageBox(str);
	sprintf(str, "%d", imageheight);
	MessageBox(str);
	sprintf(str, "%d", imagesize);
	MessageBox(str);*/

	if(m_pImageBuffer!=NULL)
       delete[] m_pImageBuffer;
 	m_pImageBuffer = new BYTE[imagesize];
	if (m_pImageBuffer) {   //分配成功
		FillMemory(m_pImageBuffer, imagesize, 0xff);   
	}
 

    capSetCallbackOnFrame(m_ghShowWnd, FrameCallbackProc);	   
  
	//get params
	CAPTUREPARMS CapParms;
	capCaptureGetSetup(m_ghShowWnd,&CapParms,sizeof (CAPTUREPARMS));
	//设置桢速
	CapParms.dwRequestMicroSecPerFrame=50000;
	//有无时间限制 
	CapParms.fLimitEnabled = FALSE;
	//是否捕捉音频
	CapParms.fCaptureAudio = FALSE;
	//MCI Device支持
	CapParms.fMCIControl = FALSE;
	//设置窗口,如果为false,捕捉画面在桌面上
	CapParms.fYield = TRUE;
	//停止捕捉键设置 
	CapParms.vKeyAbort = VK_ESCAPE;
	CapParms.fAbortLeftMouse = FALSE;
	CapParms.fAbortRightMouse = FALSE; 
	capCaptureSetSetup(m_ghShowWnd,&CapParms,sizeof (CAPTUREPARMS)); 
	//设置预览时的比例
	capPreviewScale(m_ghShowWnd, 1); 
	//设置预览时的帧频率
	capPreviewRate(m_ghShowWnd,50);
	capPreviewScale(m_ghShowWnd, FALSE); 

}

void CCV_homeworkView::bitMapPaint()
{//画出图形到显示窗口上
	///CDC *pDC=GetDC();
	CClientDC *pDC=new CClientDC(FromHandle(m_ghShowWnd));
	CRect rect(m_cpoint_lefttop_pic,m_cpoint_rightbottom_pic);	
	CBitmap c_bitmap;
	BITMAP bmInfo;
	CDC dc;

	///GetWindowRect(rect);
	//GetClientRect(rect);

	if(m_bisscared)
		c_bitmap.LoadBitmap(IDB_BITMAP1);	
	else
		c_bitmap.LoadBitmap(IDB_BITMAP2);	

	c_bitmap.GetObject(sizeof(bmInfo),&bmInfo);

	dc.CreateCompatibleDC(pDC);
	dc.SelectObject(&c_bitmap);
	pDC->StretchBlt(
		rect.TopLeft().x,rect.TopLeft().y,
		rect.Width(),rect.Height(),
		&dc,
		0,0,
		bmInfo.bmWidth,bmInfo.bmHeight,
		SRCCOPY
	);
	//pDC->
	/*
	  pDC->BitBlt(100,100,
	  bmInfo.bmWidth,??bmInfo.bmHeight,
	  &dc,0,0,SRCCOPY);
	*/
	ReleaseDC(pDC);
}

//DEL void CCV_homeworkView::OnOutofmemorySliderSpeed(NMHDR* pNMHDR, LRESULT* pResult) 
//DEL {
//DEL 	// TODO: Add your control notification handler code here
//DEL 	 m_i_speed = m_slider.GetPos();
//DEL 
//DEL 	*pResult = 0;
//DEL }

void CCV_homeworkView::pic_move(CRect &rect)
{
		CPoint rtl=rect.TopLeft();
		CPoint rbr=rect.BottomRight();
		
		double iwidth=rect.Width();
		double iheighth=rect.Height();

		rbr.x=rbr.x-20;
		rbr.y=rbr.y-20;	
		
		rbr.y-=rtl.y;//窗口位置零
		rbr.x-=rtl.x;
		rtl.y=0;
		rtl.x=0;
		bool	flag=true;
		int t = 0;

		if(m_bistrace)
		{
			for(int i=0;i<20;i++)
			{
				for(int j=0;j<20;j++)
				{
					if(m_pGrayImageBuffer[(int)(imagewidth*(m_cpoint_old_tl_pic.x+i)/(iwidth)
						+imagewidth*(int)(imageheight*(m_cpoint_old_tl_pic.y+j)/(iheighth)))]<120)
						t++;
				}
				if(t>m_i_intimate)
				{
					m_i_speed=m_i_shy_speed;
					m_i_orientation=rand()%8+1;
					m_bisscared=true;
					break;
				}
			}
			if(t<=m_i_intimate)
			{
				m_i_speed=((CSliderCtrl*)GetDlgItem(IDC_SLIDER_SPEED))->GetPos();
				m_bisscared=false;
			}
		}
		else if(m_i_speed==m_i_shy_speed)
		{
			m_i_speed=((CSliderCtrl*)GetDlgItem(IDC_SLIDER_SPEED))->GetPos();
			m_bisscared=false;
		}

		while(flag){
			switch(m_i_orientation){
			//case 0:
			//	m_cpoint_lefttop_pic=m_cpoint_old_tl_pic;
			//	break;
			case 1:
				if(
					(rtl.x<=m_cpoint_old_tl_pic.x+m_i_speed)&&
					(rbr.x>=m_cpoint_old_tl_pic.x+m_i_speed)&&
					(rtl.y<=m_cpoint_old_tl_pic.y+m_i_speed)&&
					(rbr.y>=m_cpoint_old_tl_pic.y+m_i_speed)
					){
					m_cpoint_lefttop_pic.x=m_cpoint_old_tl_pic.x+m_i_speed;
					m_cpoint_lefttop_pic.y=m_cpoint_old_tl_pic.y+m_i_speed;
					flag=false;
				}else{
					m_i_orientation=rand()%8+1;
					
				}
				break;
			case 2:
				if(
					rtl.x<=m_cpoint_old_tl_pic.x-m_i_speed&&
					rbr.x>=m_cpoint_old_tl_pic.x-m_i_speed&&
					rtl.y<=m_cpoint_old_tl_pic.y-m_i_speed&&
					rbr.y>=m_cpoint_old_tl_pic.y-m_i_speed
					){
					m_cpoint_lefttop_pic.x=m_cpoint_old_tl_pic.x-m_i_speed;
					m_cpoint_lefttop_pic.y=m_cpoint_old_tl_pic.y-m_i_speed;
					flag=false;
				}else{
					m_i_orientation=rand()%8+1;
					
				}
				break;
			case 3:
				if(
					rtl.x<=m_cpoint_old_tl_pic.x+m_i_speed&&
					rbr.x>=m_cpoint_old_tl_pic.x+m_i_speed&&
					rtl.y<=m_cpoint_old_tl_pic.y-m_i_speed&&
					rbr.y>=m_cpoint_old_tl_pic.y-m_i_speed
					){
					m_cpoint_lefttop_pic.x=m_cpoint_old_tl_pic.x+m_i_speed;
					m_cpoint_lefttop_pic.y=m_cpoint_old_tl_pic.y-m_i_speed;
					flag=false;
				  }else{
					m_i_orientation=rand()%8+1;
					
				}
				break;
			case 4:
				if(
					rtl.x<=m_cpoint_old_tl_pic.x-m_i_speed&&
					rbr.x>=m_cpoint_old_tl_pic.x-m_i_speed&&
					rtl.y<=m_cpoint_old_tl_pic.y+m_i_speed&&
					rbr.y>=m_cpoint_old_tl_pic.y+m_i_speed
					){
					m_cpoint_lefttop_pic.x=m_cpoint_old_tl_pic.x-m_i_speed;
					m_cpoint_lefttop_pic.y=m_cpoint_old_tl_pic.y+m_i_speed;
					flag=false;
				   }else{
					m_i_orientation=rand()%8+1;
					
				}
				break;
			case 5:
				if(
					rtl.x<=m_cpoint_old_tl_pic.x-m_i_speed&&
					rbr.x>=m_cpoint_old_tl_pic.x-m_i_speed
					){
					m_cpoint_lefttop_pic.x=m_cpoint_old_tl_pic.x-m_i_speed;
					m_cpoint_lefttop_pic.y=m_cpoint_old_tl_pic.y;
					flag=false;
				   }else{
					m_i_orientation=rand()%8+1;
					
				}
				break;
			case 6:
				if(
					rtl.y<=m_cpoint_old_tl_pic.y-m_i_speed&&
					rbr.y>=m_cpoint_old_tl_pic.y-m_i_speed
					){
					m_cpoint_lefttop_pic.x=m_cpoint_old_tl_pic.x;
					m_cpoint_lefttop_pic.y=m_cpoint_old_tl_pic.y-m_i_speed;
					flag=false;
				   }else{
					m_i_orientation=rand()%8+1;
					
				}	
				break;
			case 7:
				if(
					rtl.x<=m_cpoint_old_tl_pic.x+m_i_speed&&
					rbr.x>=m_cpoint_old_tl_pic.x+m_i_speed
					){
					m_cpoint_lefttop_pic.x=m_cpoint_old_tl_pic.x+m_i_speed;
					m_cpoint_lefttop_pic.y=m_cpoint_old_tl_pic.y;
					flag=false;
				   }else{
					m_i_orientation=rand()%8+1;
					
				}
				break;
			case 8:
				if(
					rtl.y<=m_cpoint_old_tl_pic.y+m_i_speed&&
					rbr.y>=m_cpoint_old_tl_pic.y+m_i_speed
					){
					m_cpoint_lefttop_pic.x=m_cpoint_old_tl_pic.x;
					m_cpoint_lefttop_pic.y=m_cpoint_old_tl_pic.y+m_i_speed;
					flag=false;
				   }else{
					m_i_orientation=rand()%8+1;
					
				}
				break;
			
			default:
				flag=false;
				break;		
			}
		}
		m_cpoint_old_tl_pic=m_cpoint_lefttop_pic;
		m_cpoint_rightbottom_pic.x=m_cpoint_lefttop_pic.x+20;
		m_cpoint_rightbottom_pic.y=m_cpoint_lefttop_pic.y+20;
		bitMapPaint();
}

void CCV_homeworkView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	m_i_speed=((CSliderCtrl*)GetDlgItem(IDC_SLIDER_SPEED))->GetPos();//取得当前位置值  
	m_i_shy_speed=((CSliderCtrl*)GetDlgItem(IDC_SLIDER_SHY))->GetPos();
	m_i_intimate=((CSliderCtrl*)GetDlgItem(IDC_SLIDER_INTIMATE))->GetPos();
	m_i_bkthreshold=((CSliderCtrl*)GetDlgItem(IDC_SLIDER_BKTHRESHOLD))->GetPos();
	CFormView::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CCV_homeworkView::OnButtonSetbktrace() 
{
	// TODO: Add your control notification handler code here
	if(!m_pImageBuffer)
	{
		MessageBox("请先按开始键");
		return;
	}
	
	int i;
	if(m_bkgImageBuffer){
		delete[] m_bkgImageBuffer;
	}
	m_bkgImageBuffer=new BYTE[imagesize/3];
//	memcpy(m_bkgImageBuffer, m_pImageBuffer, imagesize);
	
	for(i=0;i<imagesize; i+=3){
	   m_bkgImageBuffer[i/3] = BYTE(m_pImageBuffer[i]/3.0 +m_pImageBuffer[i+1]/3.0+m_pImageBuffer[i+2]/3.0) ;
	}//取背景并将背景灰度化

	m_bistrace=!m_bistrace;
	


	//UpdateData();
//	for(  i=0;i<imagesize/3 ;i++)
//	{
//		if(abs(m_pGrayImageBuffer[i] - m_bkgImageBuffer[i])< 60 )
//			m_pGrayImageBuffer[i]  = 255; 
//		else
//			m_pGrayImageBuffer[i]  = 0; 
//	}


}
