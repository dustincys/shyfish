; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCV_homeworkView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CV_homework.h"
LastPage=0

ClassCount=5
Class1=CCV_homeworkApp
Class2=CCV_homeworkDoc
Class3=CCV_homeworkView
Class4=CMainFrame

ResourceCount=3
Resource1=IDR_MAINFRAME
Resource2=IDD_ABOUTBOX
Class5=CAboutDlg
Resource3=IDD_CV_HOMEWORK_FORM

[CLS:CCV_homeworkApp]
Type=0
HeaderFile=CV_homework.h
ImplementationFile=CV_homework.cpp
Filter=N

[CLS:CCV_homeworkDoc]
Type=0
HeaderFile=CV_homeworkDoc.h
ImplementationFile=CV_homeworkDoc.cpp
Filter=N

[CLS:CCV_homeworkView]
Type=0
HeaderFile=CV_homeworkView.h
ImplementationFile=CV_homeworkView.cpp
Filter=D
BaseClass=CFormView
VirtualFilter=VWC
LastObject=CCV_homeworkView


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T




[CLS:CAboutDlg]
Type=0
HeaderFile=CV_homework.cpp
ImplementationFile=CV_homework.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_VIEW_TOOLBAR
Command15=ID_VIEW_STATUS_BAR
Command16=ID_APP_ABOUT
CommandCount=16

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_CV_HOMEWORK_FORM]
Type=1
Class=CCV_homeworkView
ControlCount=12
Control1=IDC_STATIC_SHOW,static,1342177287
Control2=IDC_BUTTON_BEGIN,button,1342242816
Control3=IDC_SLIDER_SPEED,msctls_trackbar32,1342242840
Control4=IDC_STATIC,static,1342308352
Control5=IDC_BUTTON_SETBKTRACE,button,1342242816
Control6=IDC_SLIDER_SHY,msctls_trackbar32,1342242840
Control7=IDC_STATIC,static,1342308352
Control8=IDC_SLIDER_INTIMATE,msctls_trackbar32,1342242840
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,button,1342177287
Control11=IDC_SLIDER_BKTHRESHOLD,msctls_trackbar32,1342242840
Control12=IDC_STATIC,static,1342308352

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

