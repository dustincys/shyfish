﻿// CV_homework.h : main header file for the CV_HOMEWORK application
//

#if !defined(AFX_CV_HOMEWORK_H__758A27CC_B8D5_4563_BBC8_9004963E8609__INCLUDED_)
#define AFX_CV_HOMEWORK_H__758A27CC_B8D5_4563_BBC8_9004963E8609__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCV_homeworkApp:
// See CV_homework.cpp for the implementation of this class
//
#define WM_VIDEO_EX_CHANGE		(WM_USER + 101)
#define WM_HSCHROLL				(WM_USER + 102)

class CCV_homeworkApp : public CWinApp
{
public:
	CCV_homeworkApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCV_homeworkApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCV_homeworkApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CV_HOMEWORK_H__758A27CC_B8D5_4563_BBC8_9004963E8609__INCLUDED_)
